LINKER      = g++
LINKERFLAGS = -s
CXX         = g++
CXXFLAGS    = -O3 -Wall -std=c++14 -Wextra
BIN         = test-digit
vpath %.o build
OBJ         = test_digit_to_int.o digit_to_int.o
LINKOBJ     = build/test_digit_to_int.o build/digit_to_int.o

.PHONY: all all-before all-after clean clean-custom

all: all-before $(BIN) all-after

clean: clean-custom
	rm -f ./build/*.o
	rm -f ./build/$(BIN)

.cpp.o:
	$(CXX) -c $< -o $@ $(CXXFLAGS)
	mv $@ ./build

$(BIN):$(OBJ)
	$(LINKER) -o $(BIN) $(LINKOBJ) $(LINKERFLAGS)
	mv $(BIN) ./build
