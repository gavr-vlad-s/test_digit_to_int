/*
    File:    test_digit_to_int.cpp
    Created: 17 March 2019 at 15:09 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/

#include <cstdio>
#include <utility>
#include <string>
#include "digit_to_int.h"
#include "testing.hpp"

static const char* msg_fmt = "Integer value corresponding to digit U\'%s\' is %d\n";

static const std::pair<char32_t, std::string> digits[] = {
    {U'0', "0"}, {U'1', "1"}, {U'2', "2"}, {U'3', "3"},
    {U'4', "4"}, {U'5', "5"}, {U'6', "6"}, {U'7', "7"},
    {U'8', "8"}, {U'9', "9"}, {U'A', "A"}, {U'B', "B"},
    {U'C', "C"}, {U'D', "D"}, {U'E', "E"}, {U'F', "F"},
    {U'a', "a"}, {U'b', "b"}, {U'c', "c"}, {U'd', "d"},
    {U'e', "e"}, {U'f', "f"}
};

using Pair_for_test = std::pair<char32_t, unsigned>;

static const Pair_for_test tests[] = {
    {U'f', 15}, {U'A', 10}, {U'0', 0 }, {U'd', 13},
    {U'7', 7 }, {U'c', 12}, {U'5', 5 }, {U'a', 10},
    {U'3', 3 }, {U'F', 15}, {U'9', 9 }, {U'1', 1 },
    {U'E', 14}, {U'8', 8 }, {U'2', 2 }, {U'e', 14},
    {U'B', 11}, {U'4', 4 }, {U'D', 13}, {U'b', 11},
    {U'6', 6 }, {U'C', 12},
};

int main()
{
    for(const auto& d : digits){
        printf(msg_fmt, d.second.c_str(), digit_to_int(d.first));
    }

    puts("\nRunning tests for conversion...");
    testing::test(std::begin(tests), std::end(tests), digit_to_int);
}